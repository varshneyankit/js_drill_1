// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModels(inventory) {
  if (Array.isArray(inventory)) {
    const sortedCarModels = [];

    for (let index = 0; index < inventory.length; index++) {
      sortedCarModels.push(inventory[index].car_model);
    }

    sortedCarModels.sort();

    return sortedCarModels;
  } else {
    return [];
  }
}

module.exports = sortCarModels;
