const inventory = require("../data");
const getCarById = require("../problem1");

const car = getCarById(inventory, 33);

if (car != null) {
  console.log(
    `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`
  );
} else {
  console.log("Car not found");
}
