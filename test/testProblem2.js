const inventory = require("../data");
const getLastCar = require("../problem2");

const car = getLastCar(inventory);

if (car != null) {
  console.log(`Last car is a ${car.car_make} ${car.car_model}`);
} else {
  console.log("Car not found");
}
