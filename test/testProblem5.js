const inventory = require("../data");
const getOlderCars = require("../problem5");

const olderCars = getOlderCars(inventory, 2000);

if (olderCars.length != 0) {
  console.log(olderCars);
} else {
  console.log("Data is empty");
}
