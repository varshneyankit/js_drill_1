const inventory = require("../data");
const getCarYears = require("../problem4");

const carYears = getCarYears(inventory);

if (carYears.length != 0) {
  console.log(carYears);
} else {
  console.log("Data is empty");
}
