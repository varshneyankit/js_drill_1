const inventory = require("../data");
const sortCarModels = require("../problem3");

const sortedCarModels = sortCarModels(inventory);

if (sortedCarModels.length != 0) {
  console.log(sortedCarModels);
} else {
  console.log("Data is empty");
}
