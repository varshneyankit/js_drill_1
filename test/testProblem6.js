const inventory = require("../data");
const getBMWAndAudiCars = require("../problem6");

const BMWAndAudi = getBMWAndAudiCars(inventory);

if (BMWAndAudi.length != 0) {
  console.log(JSON.stringify(BMWAndAudi));
} else {
  console.log("Data is empty");
}
