const getCarYears = require("./problem4");
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function getOlderCars(inventory, givenYear) {
  if (Array.isArray(inventory) && typeof givenYear == "number") {
    const olderCars = [];
    const carYears = getCarYears(inventory);

    for (let index = 0; index < carYears.length; index++) {
      let currentYear = carYears[index];
      if (currentYear < givenYear) {
        olderCars.push(currentYear);
      }
    }

    return olderCars;
  } else {
    return [];
  }
}

module.exports = getOlderCars;
